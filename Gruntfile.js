module.exports = function(grunt){

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        // production 
        /*cssc: {
            build: {
                options: {
                    consolidateViaDeclarations: true,
                    consolidateViaSelectors:    true,
                    consolidateMediaQueries:    true
                },
                files: {
                    'site/assets/css/master.css': 'site/assets/css/master.css'
                }
            }
        },
        cssmin: {
            build: {
                src: 'site/assets/css/master.css',
                dest: 'site/assets/css/master.css'
            }
        },
        uglify: {
            build: {
                files: {
                    'assets/js/base.min.js': ['assets/js/base.js']
                }
            }
        }*/
        
        /*
        |
        |  DEV
        |  
        */ 

        // 1. compile sass
        sass: {  
            options: {     
                    compass: true,                  // Target options
                    //compass: false,
                    style: 'expanded'
            },                            // Task
            dist: {                            // Target
                files: {                         // Dictionary of files
                    'assets/css/default.noprefix.css': 'assets/sass/default.sass', // 'destination': 'source'
                    'assets/css/smart.noprefix.css': 'assets/sass/smart.sass' // 'destination': 'source'
                }
            }
        },

        // 2. add vendor-specific prefixes from Can I Use
        autoprefixer: {
            default: {
                src: "assets/css/default.noprefix.css",
                dest: "site/assets/css/default.css"
            },
            smart:{
                src: "assets/css/smart.noprefix.css",
                dest: "site/assets/css/smart.css"
            }
        },


        // 3. copy contents of CSS folder to /site (includes source maps)
        copy: {

            // css_to_frontend: {
            //     cwd: "assets/css/",
            //     expand: true,
            //     src: "** ",
            //     dest: "site/assets/css/",
            //     flatten: true
            // },

            js_to_frontend: {
                cwd: "assets/js",
                src: "** ",
                expand: true,
                dest: "site/assets/js",
                flatten: false
            } ,

            fonts_to_frontend: {
                cwd: "assets/fonts",
                src: "** ",
                expand: true,
                dest: "site/assets/fonts",
                flatten: false
            }, 

            images_to_frontend:{
                expand: true,
                cwd: "assets/img", 
                src: "**",
                dest: "site/assets/img"
            }


        },


        // create retina & non-retina sprite sheets, place in site/assets/img
        css_sprite: {
            options: {
                cssPath: "assets/img/sprite", // checked
                orientation: "binary-tree",
                processor: "sass",
                margin: 4,
                interpolation: "linear"
            },
            sprite: {
                options: {
                    style: "assets/sass/_sprite.sass", 
                    prefix: "icon",
                    retina: true
                },
                src: ["assets/img/sprite/*"],
                dest: ["site/assets/img/sprite"]
            }
        },

        // configure live reload
        livereload  : {
            options   : {
              base    : 'site',
            },
            files     : ['site/**/*']
            //files      : ['site/assets/css/**/*']
        },
        

        watch: {

            options: { livereload: true },

            css: {
                //files: ["assets/sass/**/*", "assets/sprites/**/*"],
                files: ["assets/sass/**/*"],
                //tasks: ["css"]//, "clean:post"]
                tasks: ['sass', 'autoprefixer', "copy:fonts_to_frontend", "copy:js_to_frontend"]
            },

            fonts: {
                files: ["assets/fonts/**/*"], 
                tasks: ["copy:fonts_to_frontend"]
            }, 

            js: {
                files: ["assets/js/**/*"], 
                tasks: ["copy:js_to_frontend"]
            }, 

            img: {
                files: ["assets/img/**/*"], 
                tasks: ["copy:images_to_frontend"]
            }, 

            php: {
                files: ["site/application/views/*.php"],
                options: {
                    livereload: true
                }
            }

        }

    });
    
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    require("matchdep").filterDev("css-sprite").forEach(grunt.loadNpmTasks);
    //require("matchdep").filterDev("grunt-responsive-images").forEach(grunt.loadNpmTasks); // 
    grunt.registerTask("default",["css"]);    
    grunt.registerTask("css", ["copy:js_to_frontend", "css_sprite", "sass", "autoprefixer", "livereload"]);
    // "copy:css_to_frontend",

    //grunt.registerTask('buildcss',  ['compass', 'cssc', 'cssmin']);

};
