var desktopBreak		= 1024, 
	tabletBreak			= 700,
	smartBreak 			= 320,
	screenWidth 		= 0,
	screenHeight		= 0,
	screenType 			= 'feature';

$(document).ready(function(){

		function resetScreenVars() { 
			screenWidth = $(window).width();
			screenHeight = $(window).height();

			if(screenWidth>tabletBreak) {
				screenType = 'desktop';
			} else if (screenWidth>=smartBreak) {
				screenType = 'mobile';
			} else {
				screenType = 'feature';
			}
		}

		function responsiveImages(){
			resetScreenVars();
			$('img.responsive').each(function(){
				var src = $(this).attr('src');
				src = src.replace(/(mobile|feature|desktop)/, screenType);
				$(this).attr('src', src);
			});
		}

		if($('img.responsive').length){
			$(window).resize(responsiveImages);
			responsiveImages();
		}

});